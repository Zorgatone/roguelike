#include <curses.h>

void start(void);
void mainLoop(void);
void end(void);

int main(int argc, char *argv[]) {
  
  start();
  mainLoop();
  end();
  
  return 0;
}

void start(void) {
  initscr();
  raw();
  noecho();
  keypad(stdscr, TRUE);
}

void mainLoop(void) {
  do {
    clear();
    move(0,0);
    // Do stuff here
    printw("Hello, World!");
    refresh();
  } while(getch() != KEY_F(2));
}

void end(void) {
  // Free memory here
  endwin();
}
