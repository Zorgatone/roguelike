SRCDIR=./src
BINDIR=./bin
PROJECT_NAME=roguelike
EXEFILE=$(PROJECT_NAME).exe
EXEPATH=$(BINDIR)/$(EXEFILE)
CC=gcc
LIBS=-lcurses
RM=rm -rf

$(EXEPATH): ./src/main.c
	$(CC) ./src/main.c -o $(EXEPATH) $(LIBS)

clean:
	$(RM) $(BINDIR)/*.o $(BINDIR)/*.out $(BINDIR)/*.a $(BINDIR)/*.exe $(BINDIR)/*.lib $(BINDIR)/*.s $(BINDIR)/*.dll ./src/*~ ./*~
